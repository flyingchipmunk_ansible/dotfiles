# dotfiles

Ansible role for deploying dot-bot based dotfiles, https://git.io/dotbot

## Requirements

* Ansible >= 2.2
* git >= 1.7.1 on the target hosts

## Role Variables

Enable/disable the entire role:

    dotfiles_enabled: true|false

Path to SSH deploy private key file:

    dotfiles_deploy_key_file: ~/.ssh/your_deploy_key

Directory to store source code:

    dotfiles_src_dir: ~/src

'dotfiles' repo and branch to use:

    dotfiles_repo: git@gitlab.com:username/dotfiles.git
    dotfiles_branch: master

If you want to also use 'dotfiles-local' or not. See: https://github.com/anishathalye/dotbot/pull/11#issuecomment-73082152

    dotfiles_use_dotfiles_local: true|false

'dotfiles-local' repo and branch to use:

    dotfiles_local_repo: git@gitlab.com:username/dotfiles-local.git
    dotfiles_local_branch: master

## Dependencies

None.

## Example Playbook

    - name: Setup dotfiles
      import_role:
        name: dotfiles
      when: dotfiles_enabled

## License

The MIT License (MIT)

Copyright (c) 2018 Matthew C. Veno

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

https://opensource.org/licenses/MIT

## Author Information

Matthew C. Veno
